# README #

# Introduction to programming econometrics with R #

This book is primarily intended for third year students of the Quantitative Economics section at the faculty of economics from the University of Strasbourg, France. The goal is to teach them the basics of programming with R, and applying this knowledge to solve problems in economics, finance and marketing. You are free to redistribute free copies of this book. You are also free to modify, remix, transform or adapt the contents of this book, but please, give appropriate credit if you do use this book.

You can compile the book in two ways: first download the whole repository:

`git clone git@bitbucket.org:b-rodrigues/programmingeconometrics.git`

and then, with Rstudio, open the file *appliedEconometrics.Rnw* and knit it. Another solution is to use the Makefile. Just type:

`make`

inside a terminal (should work on GNU+Linux and OSX systems) and it should compile the document. You may get some message about "additional information" for some R packages. When these come up, just press Q on your keyboard to continue the compilation process.
